from dogm204 import ssd1803a_spi
from pins import pins


dd = ssd1803a_spi(clk=pins['clk'], miso=pins['miso'], mosi=pins['mosi'], reset=pins['reset'])

dd.cmd_clear()
dd.dis_print_lines(['Hello World',
                    '       Line 1       ',
                    '       Line 2       ',
                    '       Line 3       '])
