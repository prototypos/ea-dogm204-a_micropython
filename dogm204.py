"""
This script is based on the work (Copyright (c) 2015 gzink) in the following repository : https://github.com/gzink/displayserver
It has been modified to be used with micropython
"""

import machine
import time


class ssd1803a_spi():
    MISOPIN = None
    MOSIPIN = None
    CLKPIN = None
    CSPIN = None  # not used really, use only if needed
    RESET = None  # suggested after startup

    def __init__(self, miso, mosi, clk, reset=27, cs=14):
        # self._cs = machine.Pin(self.CSPIN, mode=machine.Pin.OUT, value =1) #activate if with cs needed
        self.MISOPIN = miso
        self.MOSIPIN = mosi
        self.CLKPIN = clk
        self.RESET = reset
        self.CSPIN = cs

        self._spi = machine.SoftSPI(baudrate=1000000, polarity=0,
                                    phase=0, bits=8,
                                    sck=machine.Pin(self.CLKPIN, machine.Pin.OUT, machine.Pin.PULL_DOWN),
                                    mosi=machine.Pin(self.MOSIPIN, machine.Pin.OUT, machine.Pin.PULL_UP),
                                    miso=machine.Pin(self.MISOPIN, machine.Pin.IN, machine.Pin.PULL_UP))

        self._reset = machine.Pin(self.RESET, machine.Pin.OUT, machine.Pin.PULL_UP)
        self._reset.value(1)
        # from manual (EA DOGM204-A)
        init = [0x3a, 0x09, 0x06, 0x1e, 0x39, 0x1b, 0x6e, 0x57, 0x7f, 0x38, 0x0c, 0x01]

        # reset the module
        time.sleep_us(500)
        self.reset()

        for byte in init:
            self.send_byte(byte)

    def send_byte(self, byte, write=0, new_char=0, pos=0, lines=0):
        if write > 0:
            cmd = 0xfa
        else:
            cmd = 0xf8

        if new_char == 1:
            cmd = 0x80 | pos << 3 | lines

        byte = self.reversebits(byte)
        print([cmd, byte & 0xf0, (byte & 0x0f) << 4])  # can be omitted

        buf = bytes([cmd, byte & 0xf0, (byte & 0x0f) << 4])
        # self._cs.value(0) #use only if used
        self._spi.write(buf)
        # self._cs.value(1)

    def new_char(self, asciicode, lines):

        if asciicode > 7:
            asciicode = 7

        if len(lines):
            lines = lines[0:8]

        for i in lines:
            self.sent_byte(lines, 1, 1, asciicode, i)

    def reversebits(self, byte):
        byte = ((byte & 0xF0) >> 4) | ((byte & 0x0F) << 4)
        byte = ((byte & 0xCC) >> 2) | ((byte & 0x33) << 2)
        byte = ((byte & 0xAA) >> 1) | ((byte & 0x55) << 1)
        return byte

    def cmd_clear(self):
        self.send_byte(0x01)

    def reset(self):
        self._reset.value(0)
        time.sleep_us(200)
        self._reset.value(1)
        time.sleep_us(200)

    def dis_print_lines(self, lines):
        self.cmd_clear()

        for line in lines:
            line = line[0:20]
            self.dis_print(line)
            if len(line) < 20:
                self.dis_print(' ' * (20 - len(line)))

    def dis_print(self, str):
        for chr in list(str):
            self.send_byte(ord(chr), 1)




