# EA DOGM204
Usages of the display EA DOGM204-A using SPI
- MicroPython (v1.19.1)
- Tested on ESP32

## Usage:
SoftSPI is being used with max. speed of LCD-Display

main.py displays simple text on 4 lines


## Definition of Pins for Bus:
create a "pins.py" with the following format : 
```doctest
pins = {
    "clk": 4,
    "miso": 22,
    "mosi": 21,
    "reset": 2,
}
```
replace the numbers with your connections.

### Note
Only simple character display is implemented. New character functionnality not implemented (yet)
